import 'package:flutter/material.dart';

enum TransactionType { salary, income, expense }

class Transaction {
  String title;
  String description;
  double price;
  String sign = '-'; // 0 = -, 1 = +, this is the sign next to the number
  IconData icon;
  MaterialColor color;
  TransactionType type;

  Transaction(this.title, this.description, this.price, this.sign, this.icon, this.color, this.type);
}

String truncate(String text, { length = 20, omission = '...' }) {
  if (length >= text.length) {
    return text;
  }
  return text.replaceRange(length, text.length, omission);
}

