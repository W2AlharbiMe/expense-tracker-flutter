import 'package:expense_tracker_two/add_transaction_form.dart';
import 'package:expense_tracker_two/transaction.dart';
import 'package:expense_tracker_two/privacy_policy.dart';
import 'package:expense_tracker_two/transaction_details_page.dart';
import 'package:flutter/material.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Expense Tracker',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const MyHomePage(title: 'Expense Tracker'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Transaction> tx = [];

  double salary = 0;
  double expenses = 0;
  double balance = 0;
  double income = 0;


  void add(Transaction a) {
    setState(() {
      tx.insert(0, a);
    });
  }

  void setSalary(double value) {
    setState(() {
      salary = value;
    });
  }

  void setExpenses(double value) {
    setState(() {
      expenses = value;
    });
  }

  void setBalance(double value) {
    setState(() {
      balance = value;
    });
  }

  void setIncome(double value) {
    setState(() {
      income = value;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: IconButton(onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PrivacyPolicy()));
              }, icon: const Icon(Icons.privacy_tip), tooltip: 'Privacy Policy',),
            )
          ],
        ),
        body: Column(
          children: [

            const SizedBox(
              child: Text(''),
            ),

            // analytics --------------------------
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    const Text('Monthly Salary',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    Container(
                      color: Colors.green[800],
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        salary.toString(),
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),


                Column(
                  children: [
                    const Text('Expenses',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    Container(
                      color: Colors.red,
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        expenses.toString(),
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                )
              ],
            ),


            const SizedBox(height: 20,),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    const Text('Current Balance',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    Container(
                      color: Colors.green,
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        balance.toString(),
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),

                Column(
                  children: [
                    const Text('Income',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    Container(
                      color: Colors.teal,
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        income.toString(),
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                )
              ],
            ),

            // /analytics --------------------------

            Container(
              margin: const EdgeInsets.all(20.0),
              child: const Text(
                'Transactions',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            ),

            // ElevatedButton(onPressed: clear, child: const Text('Clear')),

            // Transactions List ----------------------------

            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: tx.length,
                itemBuilder: (context, index) {
                  var t = tx[index];
                  return ListTile(
                    leading: Icon(t.icon),
                    iconColor: t.color,
                    subtitle: Text(truncate(t.description == '' ? 'No Description' : t.description)),
                    trailing: Text("${t.sign}${t.price.toString()}", style: TextStyle(
                      color: t.color
                    )),
                    title: Text(truncate(t.title, length: 30)),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => TransactionDetailsPage(t: t)));
                    },
                  );
                },
              ),
            ),

            // /transactions List ----------------------------

          ],
        ),

        floatingActionButton: Padding(
          padding: const EdgeInsets.only(bottom: 25),
          child: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddTransactionForm(
                    add: add,
                    setBalance: setBalance,
                    setExpenses: setExpenses,
                    setIncome: setIncome,
                    setSalary: setSalary,

                    balance: balance,
                    expenses: expenses,
                    income: income,
                    salary: salary,
                ),
                settings: RouteSettings(
                  arguments: tx
                ))
              );
            },
            tooltip: 'Add new Transaction',
            child: const Icon(Icons.add),
          ),
        ),
        );
  }
}

