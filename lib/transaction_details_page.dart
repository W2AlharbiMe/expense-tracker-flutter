import 'package:expense_tracker_two/privacy_policy.dart';
import 'package:expense_tracker_two/transaction.dart';
import 'package:flutter/material.dart';

class TransactionDetailsPage extends StatelessWidget {
  const TransactionDetailsPage({Key? key, required this.t}) : super(key: key);

  final Transaction t;


  String transactionType() {
    if(t.type == TransactionType.salary) return 'Salary';
    if(t.type == TransactionType.expense) return 'Expense';

    return 'Income';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Transaction Details'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: IconButton(onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PrivacyPolicy()));
            }, icon: const Icon(Icons.privacy_tip), tooltip: 'Privacy Policy',),
          )
        ],
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(height: 20),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  t.icon,
                  color: t.color,
                  size: 36,
                ),

                Text(
                  '${t.sign}${t.price}',
                  style: TextStyle(
                    color: t.color,
                    fontWeight: FontWeight.bold,
                    fontSize: 26,
                  ),
                ),
              ],
            ),

            Text(
              t.title.toUpperCase(),
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 26,
              ),
            ),

            Text(
              'Type: ${transactionType()}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),

            const SizedBox(height: 25,),

            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Text(
                t.description == '' ? 'No Description' : t.description,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
