import 'package:flutter/material.dart';




class PrivacyPolicy extends StatelessWidget {
  const PrivacyPolicy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Privacy Policy')),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: const [
            Text('Developer Statement', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),),
            SizedBox(height: 20),
            Text('this page describes how this mobile app DOES NOT collect any personal data, it DOES NOT use the network, it DOES NOT view/edit/delete files, it DOES NOT need any kind of permissions, all the data(transaction type(salary, income, expense), transaction title, transaction description, transaction price) are saved in memory for a temporary use.'),
            SizedBox(height: 20),

            Text('1. this is an open-source project accessible in GitLab via the following link:'),

            SizedBox(height: 20),

            SizedBox(width: 400, child: Text('https://gitlab.com/W2AlharbiMe/expense-tracker-flutter'),),

            SizedBox(height: 20),

            SizedBox(width: 400, child: Text('2. this project is created for educational purposes.')),

            SizedBox(height: 20),

            Text('3. transactions data are saved in memory and will be removed after closing the mobile app.'),
          ],
        ),
      ),
    );
  }
}
