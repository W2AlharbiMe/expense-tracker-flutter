import 'package:expense_tracker_two/transaction.dart';
import 'package:expense_tracker_two/privacy_policy.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconpicker/flutter_iconpicker.dart';

class AddTransactionForm extends StatefulWidget {
  const AddTransactionForm({Key? key,
      required this.add,
      required this.setSalary,
      required this.setExpenses,
      required this.setBalance,
      required this.setIncome,
      required this.salary,
      required this.expenses,
      required this.balance,
      required this.income,
  }) : super(key: key);

  final Function add;
  final Function setSalary;
  final Function setExpenses;
  final Function setBalance;
  final Function setIncome;

  // for read-only do not change !!!!
  // use the methods to trigger rendering !!!!!
  final double salary;
  final double expenses;
  final double balance;
  final double income;

  @override
  State<AddTransactionForm> createState() => _AddTransactionFormState();
}



class _AddTransactionFormState extends State<AddTransactionForm> {

  final titleController = TextEditingController();
  final priceController = TextEditingController();
  final descriptionController = TextEditingController();
  TransactionType? _type = TransactionType.expense;
  Icon? _icon;
  IconData? _iconData;

  _pickIcon() async {
    IconData? icon = await FlutterIconPicker.showIconPicker(context,
        iconPackModes: [IconPack.material]);

    _icon = Icon(icon, size: 36,);
    _iconData = icon;

    setState(() {});
  }



  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    titleController.dispose();
    priceController.dispose();
    descriptionController.dispose();

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // print(widget.tx);
    // final tx = ModalRoute.of(context)!.settings.arguments as List<Transaction>;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Add new Transaction'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: IconButton(onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PrivacyPolicy()));
            }, icon: const Icon(Icons.privacy_tip), tooltip: 'Privacy Policy',),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                children: [



                  (() {
                    if(_type == TransactionType.expense) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const SizedBox(
                            height: 20.0,
                          ),

                          ElevatedButton(
                            onPressed: _pickIcon,
                            child: const Text('Choose Icon'),
                          ),

                          AnimatedSwitcher(
                            duration: const Duration(milliseconds: 300),
                            child: _icon ?? Container(),
                          ),
                        ],
                      );
                    }

                    return const Text('');
                    // return ElevatedButton(
                    //   onPressed: _pickIcon,
                    //   child: const Text('Choose Icon'),
                    // )


                  })(),


                  const SizedBox(
                    height: 20.0,
                  ),

                  Column(
                    children: [
                      const Text(
                        'Transaction Type',
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold
                        ),
                      ),

                      ListTile(
                        title: const Text('Expense'),
                        leading: Radio<TransactionType>(
                          value: TransactionType.expense,
                          groupValue: _type,
                          onChanged: (TransactionType? value) {
                            setState(() {
                              _type = value;
                            });
                          },
                        ),
                        onTap: () {
                          setState(() {
                            _type = TransactionType.expense;
                          });
                        },
                      ),

                      ListTile(
                        title: const Text('Salary'),
                        leading: Radio<TransactionType>(
                          value: TransactionType.salary,
                          groupValue: _type,
                          onChanged: (TransactionType? value) {
                            setState(() {
                              _type = value;
                            });
                          },
                        ),
                        onTap: () {
                          setState(() {
                            _type = TransactionType.salary;
                            _icon = null;
                          });
                        },
                      ),

                      ListTile(
                        title: const Text('Income'),
                        leading: Radio<TransactionType>(
                          value: TransactionType.income,
                          groupValue: _type,
                          onChanged: (TransactionType? value) {
                            setState(() {
                              _type = value;
                            });
                          },
                        ),
                        onTap: () {
                          setState(() {
                            _type = TransactionType.income;
                            _icon = null;
                          });
                        },
                      ),




                    ],
                  ),


                  SizedBox(
                    width: 350.0,
                    child: Column(
                      children: [
                        TextField(
                          controller: titleController,
                          decoration: const InputDecoration(
                            border:  OutlineInputBorder(),
                            labelText: 'Title',
                          ),
                        ),

                        const SizedBox(height: 10.0),

                        TextField(
                          controller: priceController,
                          decoration: const InputDecoration(
                              border:  OutlineInputBorder(),
                              labelText: 'Price'
                          ),
                        ),

                        const SizedBox(height: 10.0),

                        TextField(
                          controller: descriptionController,
                          maxLines: 10,
                          decoration: const InputDecoration(
                            border:  OutlineInputBorder(),
                            labelText: 'Description',
                          ),
                        ),


                        const SizedBox(height: 10),

                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          const up = Icons.arrow_upward;
          const green = Colors.green;
          const red = Colors.red;
          const down = Icons.arrow_downward;

          var t = Transaction(titleController.text, descriptionController.text, double.parse(priceController.text), '+', Icons.arrow_downward, Colors.red, _type!);


          if(_type == TransactionType.salary) {
            widget.setSalary(t.price);
            t.color = green;
            t.icon = up;

            widget.setBalance(
                widget.balance + t.price
            );
          }

          if(_type == TransactionType.income) {
            widget.setIncome(
              widget.income + t.price
            );
            t.color = green;
            t.icon = up;

            widget.setBalance(
                widget.balance + t.price
            );
          }

          if(_type == TransactionType.expense) {
            widget.setExpenses(
                widget.expenses + t.price
            );
            t.color = red;
            t.sign = '-';

            if(_iconData != null) {
              t.icon = _iconData!;
            } else {
              t.icon = down;
            }


            widget.setBalance(
              widget.balance - t.price
            );
          }

          widget.add(t);

          Navigator.pop(context);
        },
        tooltip: 'Add new Transaction',
        child: const Icon(Icons.add),
      ),
    );
  }
}

