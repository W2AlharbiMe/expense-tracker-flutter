#### Expense Tracker Flutter
---


### Privacy Policy
<strong>
this section describes how this mobile app DOES NOT collect any personal data, it DOES NOT use the network, it DOES NOT view/edit/delete files, it DOES NOT need any kind of permissions,
all the data(transaction type(salary, income, expense), transaction title, transaction description, transaction price) are saved in memory for a temporary use.

<br>


1. this is an open-source project accessible in GitLab.
2. this project is created for educational purposes.
3. transactions data are saved in memory and will be removed after closing the mobile app.
</strong>

### Outline
- Introduction
- Goal
- Used Technologies
- User Interface
- Privacy Policy


### Introduction
this mobile app let you track your expenses/income, it saves all the data locally.


### Goal
this project was created to graduate from mobile development course at CTI.


### Used Technologies
- Flutter




